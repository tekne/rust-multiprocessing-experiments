use capnp_calculator::calculator_capnp::calculator;
use capnp_rpc::{rpc_twoparty_capnp, twoparty, RpcSystem};
use clap::Parser;
use futures::AsyncReadExt;
use rustyline_async::Readline;
use std::io::Write;
use std::net::SocketAddr;

#[derive(Parser, Debug)]
pub struct Args {
    #[arg(short, long)]
    addr: SocketAddr,
}

#[tokio::main]
pub async fn main() -> anyhow::Result<()> {
    let args = Args::parse();
    tokio::task::LocalSet::new()
        .run_until(async move {
            let stream = tokio::net::TcpStream::connect(&args.addr).await?;
            stream.set_nodelay(true)?;
            let (reader, writer) =
                tokio_util::compat::TokioAsyncReadCompatExt::compat(stream).split();
            let rpc_network = twoparty::VatNetwork::new(
                reader,
                writer,
                rpc_twoparty_capnp::Side::Client,
                Default::default(),
            );
            let mut rpc_system = RpcSystem::new(Box::new(rpc_network), None);
            let hello_world: calculator::Client =
                rpc_system.bootstrap(rpc_twoparty_capnp::Side::Server);

            tokio::task::spawn_local(rpc_system);

            let (mut rl, mut stdout) = Readline::new(">>> ".into())?;

            loop {
                let name = rl.readline().await?;

                // let reply = if name.split_whitespace().next() == Some("bye") {
                //     let mut request = hello_world.say_goodbye_request();
                //     request.get().init_request().set_name(&name);
                //     request
                //         .send()
                //         .promise
                //         .await?
                //         .get()?
                //         .get_reply()?
                //         .get_message()?
                //         .to_string()
                // } else {
                //     let mut request = hello_world.say_hello_request();
                //     request.get().init_request().set_name(&name);
                //     request
                //         .send()
                //         .promise
                //         .await?
                //         .get()?
                //         .get_reply()?
                //         .get_message()?
                //         .to_string()
                // };

                // writeln!(stdout, "received: {}", reply)?;
            }
        })
        .await
}
