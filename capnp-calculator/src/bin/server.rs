use capnp::capability::Promise;
use capnp_calculator::calculator_capnp::calculator;
use capnp_rpc::{pry, rpc_twoparty_capnp, twoparty, RpcSystem};
use clap::Parser;
use futures::AsyncReadExt;
use std::net::SocketAddr;

#[derive(Parser, Debug)]
pub struct Args {
    #[arg(short, long)]
    addr: SocketAddr,
}

struct CalculatorImpl;

impl calculator::Server for CalculatorImpl {
    fn evaluate(
        &mut self,
        _: calculator::EvaluateParams,
        _: calculator::EvaluateResults,
    ) -> capnp::capability::Promise<(), capnp::Error> {
        capnp::capability::Promise::err(capnp::Error::unimplemented(
            "method calculator::Server::evaluate not implemented".to_string(),
        ))
    }

    fn get_instruction(
        &mut self,
        _: calculator::GetInstructionParams,
        _: calculator::GetInstructionResults,
    ) -> capnp::capability::Promise<(), capnp::Error> {
        capnp::capability::Promise::err(capnp::Error::unimplemented(
            "method calculator::Server::get_instruction not implemented".to_string(),
        ))
    }
}

#[tokio::main]
pub async fn main() -> anyhow::Result<()> {
    let args = Args::parse();

    tokio::task::LocalSet::new()
        .run_until(async move {
            let listener = tokio::net::TcpListener::bind(&args.addr).await?;
            let calculator_client: calculator::Client = capnp_rpc::new_client(CalculatorImpl);

            loop {
                let (stream, addr) = listener.accept().await?;
                stream.set_nodelay(true)?;
                println!("Got a connection from {addr}");
                let (reader, writer) =
                    tokio_util::compat::TokioAsyncReadCompatExt::compat(stream).split();
                let network = twoparty::VatNetwork::new(
                    reader,
                    writer,
                    rpc_twoparty_capnp::Side::Server,
                    Default::default(),
                );
                let rpc_system =
                    RpcSystem::new(Box::new(network), Some(calculator_client.clone().client));
                tokio::task::spawn_local(rpc_system);
            }
        })
        .await
}
