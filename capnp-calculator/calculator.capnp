@0x88205b317236a544;

interface Calculator {
    evaluate @0 (expression: Expression) -> (value: Thunk);

    struct Expression {
        union {
            literal @0: Value;
            previous @1: Thunk;
            parameter @2: UInt32;
            call @3: List(Expression);
            lambda: group {
                args @4: UInt32;
                body @5: Expression;
            }
        }
    }

    interface Thunk {
        read @0 () -> (value: Value);
    }

    struct Value {
        union {
            number @0: UInt64;
            closure @1: Function;
            error @2: Text;
        }
    }

    interface Function {
        call @0 (params: List(Value)) -> (value: Value);
    }

    getInstruction @1 (op: Text) -> (func: Function);
}