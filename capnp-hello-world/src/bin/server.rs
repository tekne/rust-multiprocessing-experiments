use capnp::capability::Promise;
use capnp_hello_world::hello_world_capnp::hello_world;
use capnp_rpc::{pry, rpc_twoparty_capnp, twoparty, RpcSystem};
use clap::Parser;
use futures::AsyncReadExt;
use std::net::SocketAddr;

#[derive(Parser, Debug)]
pub struct Args {
    #[arg(short, long)]
    addr: SocketAddr,
}

struct HelloWorldImpl;

impl hello_world::Server for HelloWorldImpl {
    fn say_hello(
        &mut self,
        params: hello_world::SayHelloParams,
        mut results: hello_world::SayHelloResults,
    ) -> Promise<(), ::capnp::Error> {
        let request = pry!(pry!(params.get()).get_request());
        let name = pry!(request.get_name());
        let message = format!("Hello there, {name}");

        results.get().init_reply().set_message(&message);

        Promise::ok(())
    }

    fn say_goodbye(
        &mut self,
        params: hello_world::SayGoodbyeParams,
        mut results: hello_world::SayGoodbyeResults,
    ) -> Promise<(), ::capnp::Error> {
        let request = pry!(pry!(params.get()).get_request());
        let name = pry!(request.get_name());
        let message = format!("Goodbye, {name}");

        results.get().init_reply().set_message(&message);

        Promise::ok(())
    }
}

#[tokio::main]
pub async fn main() -> anyhow::Result<()> {
    let args = Args::parse();

    tokio::task::LocalSet::new()
        .run_until(async move {
            let listener = tokio::net::TcpListener::bind(&args.addr).await?;
            let hello_world_client: hello_world::Client = capnp_rpc::new_client(HelloWorldImpl);

            loop {
                let (stream, addr) = listener.accept().await?;
                stream.set_nodelay(true)?;
                println!("Got a connection from {addr}");
                let (reader, writer) =
                    tokio_util::compat::TokioAsyncReadCompatExt::compat(stream).split();
                let network = twoparty::VatNetwork::new(
                    reader,
                    writer,
                    rpc_twoparty_capnp::Side::Server,
                    Default::default(),
                );
                let rpc_system =
                    RpcSystem::new(Box::new(network), Some(hello_world_client.clone().client));
                tokio::task::spawn_local(rpc_system);
            }
        })
        .await
}
