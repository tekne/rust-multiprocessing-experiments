@0xf26313750f4b51d4;

interface HelloWorld {
    struct HelloRequest {
        name @0: Text;
    }

    struct HelloReply {
        message @0: Text;
    }

    sayHello @0 (request: HelloRequest) -> (reply: HelloReply);
    sayGoodbye @1 (request: HelloRequest) -> (reply: HelloReply);
}